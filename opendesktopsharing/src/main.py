#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import sys
import time
from optparse import OptionParser

from opendesktopsharing import APP_NAME
from opendesktopsharing import __version__
from opendesktopsharing import __accepted_versions__
from opendesktopsharing.src.lib.commons import logging_setup
from opendesktopsharing.src.lib.settings import Settings


__all__ = ['main']


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]

    parser = OptionParser('usage: %prog [options]')

    parser.add_option(
        '--version',
        action='store_true',
        help='Show version'
    )

    parser.add_option(
        '--debug',
        action='store_true',
        help='Enable debug mode'
    )

    parser.add_option(
        '--info',
        action='store_true',
        help='Enable info mode'
    )

    parser.add_option(
        '--config',
        action='store',
        help='Config file'
    )

    parser.add_option(
        '--server',
        action='store_true',
        help='Run server mode'
    )

    parser.add_option(
        '--host',
        action='store_true',
        help='Run host mode only (cli)'
    )

    parser.add_option(
        '--guest',
        action='store',
        help='ID Password separate by "-" ex: 1234-5678 (Run guest mode only)'
    )

    options, arguments = parser.parse_args(argv)

    if options.version:
        print("""{} {}\nServer accepted versions: {}""".format(APP_NAME, __version__, __accepted_versions__))
        sys.exit(0)

    logging_setup(options)
    app_mode = 'full_gui'
    if options.server:
        app_mode = 'server'
    elif options.guest:
        app_mode = 'guest'
    elif options.host:
        app_mode = 'host'

    settings = Settings(options.config, app_mode=app_mode)

    if options.server:
        # Server mode
        import signal

        from opendesktopsharing.src.lib.server import SimpleWebSocketServer
        from opendesktopsharing.src.lib.server import OpenDesktopSharingServer

        def run_server():
            server = SimpleWebSocketServer(
                settings.server['listening_interface'],
                settings.server['port'],
                OpenDesktopSharingServer,
                settings,
            )

            def close_sig_handler(signum, frame):
                print(signal.Signals(signum).name)
                server.close()
                print('Bye')
                sys.exit(0)
            catchable_sigs = set(signal.Signals) - {signal.SIGKILL, signal.SIGSTOP}
            for sig in catchable_sigs:
                signal.signal(sig, close_sig_handler)

            server.serveforever()

        run_server()

    else:
        if options.host:
            # Only host mode
            from opendesktopsharing.src.lib.host import HostWebSocket
            try:
                host_websocket = HostWebSocket(
                    settings,
                    mode_cli=True
                )
                host_websocket.daemon = True
                host_websocket.start()
                while host_websocket.running and host_websocket.error is None:
                    time.sleep(.2)
            except (KeyboardInterrupt, SystemExit) as e:
                host_websocket.stop()

        elif options.guest:
            # Only Guest mode
            import wx
            from opendesktopsharing.src.lib.guest import GuestWebSocket
            from opendesktopsharing.src.lib.guest import ShowCapture
            id, pwd = options.guest.split('-')
            app = wx.App()
            show_capture = ShowCapture()
            guest_websocket = GuestWebSocket(
                settings,
                id=id,
                pwd=pwd,
                show_capture=show_capture,
            )
            guest_websocket.start()
            need_init = True
            while guest_websocket.running:
                time.sleep(.1)
                if need_init and guest_websocket.show_capture:
                    need_init = None
                    app.MainLoop()
            del app

            sys.exit(0)
        else:
            # Full client mode
            import wx
            from opendesktopsharing.src.lib.main_window import ConnectionWindow

            app = wx.App()
            ConnectionWindow(None, -1, "OpenDesktopSharing", settings)
            app.MainLoop()
            del app
            sys.exit(0)


if __name__ == "__main__":
    main(sys.argv)
