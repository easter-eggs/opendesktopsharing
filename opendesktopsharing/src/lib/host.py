#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

import copy
import imp
import lz4.frame
import logging
from io import BytesIO
from threading import Lock
from PIL import Image

import numpy as np
from optparse import OptionParser
import os
import platform
import pickle
import time
import subprocess
import sys
import threading
import traceback
import websocket

from pynput.keyboard import Key, HotKey,  Controller as KeyboardController
from pynput.mouse import Button, Controller as MouseController

from opendesktopsharing.src.lib.commons import logging_setup
from opendesktopsharing.src.lib.commons import humanbytes
from opendesktopsharing.src.lib.settings import Settings


log = logging.getLogger('OpenDesktopSharing')

mouse = MouseController()


class GrabImage():
    def __init__(self):
        import tempfile
        import os
        self.f = tempfile.NamedTemporaryFile()
        log.debug("##############################")
        log.debug(self.f.name)
        self.grab = None
        self.not_compatible = False
        self.backend = None
        self.childprocess = None

        test_image = None
        try:
            log.debug('Try PIL')
            from PIL import Image, ImageGrab
            self.ImageGrab = ImageGrab
            self.grab = self.grab_pil
            start = time.time()
            test_image = self.grab_pil()
            end = time.time()
            elapsed = end-start
            if elapsed > .4:
                log.warning('PIL is too slow here: {}'.format(elapsed))
                raise Exception
        except Exception as e:
            log.warning(e)
            test_image = None
            print('PIL failed')

        if test_image:
            log.debug('Success, use PIL')
        else:
            log.debug('Try pyscreenshot')
            import pyscreenshot as ImageGrab
            self.ImageGrab = ImageGrab

            self.childprocess = False
            for b in ImageGrab.backends():
                log.debug('Check pyscreenshot with {}'.format(b))
                try:
                    start = time.time()
                    test_image = self.ImageGrab.grab(
                        backend=b,
                        childprocess=self.childprocess,
                    )
                    end = time.time()
                    elapsed = end-start
                    if elapsed > .4:
                        log.warning('PIL is too slow here: {}'.format(elapsed))
                        raise Exception
                except Exception as e:
                    log.warning(e)
                    test_image = None
                    pass
                if test_image:
                    self.backend = b
                    self.grab = self.grab_pyscreenshot
                    log.debug('Success, use pyscreenshot with {}'.format(b))
                    break
            if not test_image:
                self.childprocess = True
                for b in ImageGrab.backends():
                    log.debug('Check pyscreenshot with {}'.format(b))
                    try:
                        start = time.time()
                        test_image = self.ImageGrab.grab(
                            backend=b,
                            childprocess=self.childprocess,
                        )
                        end = time.time()
                        elapsed = end-start
                        if elapsed > .4:
                            log.warning('PIL is too slow here: {}'.format(elapsed))
                            raise Exception
                    except Exception as e:
                        test_image = None
                        log.warning(e)
                        pass
                    if test_image:
                        self.backend = b
                        self.grab = self.grab_pyscreenshot
                        log.debug('Success, use pyscreenshot with {}'.format(b))
                        break
            if not test_image:
                log.error('Failed to get screen capture')
                self.not_compatible = True

    def grab_pil(self):
        return self.ImageGrab.grab(bbox = None)

    def grab_pyscreenshot(self):
        image = self.ImageGrab.grab(
            backend=self.backend,
            childprocess=self.childprocess,
        )
        return image

    def grab_gnome_screenshot(self):
        process = subprocess.Popen(['gnome-screenshot', '-f', self.f.name])
        stdout, stderr = process.communicate()
        image = Image.open(self.f.name)
        return image


class HostWebSocket(threading.Thread):
    def __init__(self, settings, mode_cli=None):
        super(HostWebSocket, self).__init__()
        # websocket.enableTrace(True)

        self.settings = settings

        log.debug("Check compat")
        log.debug("Python version: {}".format(sys.version_info))

        self.grab_image = GrabImage()

        self.not_compatible = self.grab_image.not_compatible
        if self.not_compatible:
            self.running = False
            return
        test_image = self.grab_image.grab()
        self.width = test_image.size[0]
        self.height = test_image.size[1]

        # Prevent strange resolution
        # self.width = self.width if self.width % 2 == 0 else self.width -  1
        # self.height = self.height if self.height % 2 == 0 else self.height - 1

        # Used to print session key in terminal in cli mode
        self.mode_cli = mode_cli

        self.ws = websocket.WebSocketApp(
            self.settings.client['auth'],
            header=[],
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open,
        )

        self.running = True
        self.forwarding = None
        self.guest_ready = None

        self.id = None
        self.pwd = None
        self.channel = None

        self.error = None

        self.lock = Lock()
        self.thread_grab_image_running = True
        self.thread_grab_image = threading.Thread(target=self.grab_image_loop, args=("Thread-1", ))

        self.ready_to_send = None
        self.last_frame_sended = None

        self.hotkeys = {}
        for hotkey_name in vars(Key):
            if not hotkey_name.startswith('_'):
                self.hotkeys[hotkey_name] = getattr(Key, hotkey_name)

    def on_message(self, *args):
        message = args[1] if len(args) == 2 else args[0]
        # log.debug('RECEIVING from websocket ({})'.format(humanbytes(len(message))))
        # DEBUG
        if not isinstance(message, bytes):
            return
        if b'SERVICE' in message:
            # COMMONS
            if b'SERVICE - CLOSE' in message:
                self.stop()
            elif b'SERVICE - FULL' in message:
                self.error = 'Server full'
                self.stop()
            elif b'SERVICE - MAX DURATION' in message:
                self.error = 'Max duration'
                self.stop()
            elif b'SERVICE - FORWARDING' in message:
                self.forwarding = True
            # HOST
            elif b'SERVICE - START HOST SESSION' in message:
                infos_connection = message.split(b' ')
                self.id = infos_connection[5].decode('utf-8')
                self.pwd = infos_connection[6].decode('utf-8')
                self.channel = infos_connection[7].decode('utf-8')
                if self.mode_cli:
                    # /!\ Do not remove this print
                    print('ID: {} Password: {}'.format(self.id, self.pwd))
            elif b'SERVICE - CONNECTED' in message:
                self.thread_grab_image.start()
                self.forwarding = True
                data_string = 'SERVICE - HOST SETTINGS ' + 'WIDTH=' + str(self.width) + ' HEIGHT=' + str(self.height) + ' SYSTEM=' + str(sys.platform)
                data_string = bytes(data_string, 'utf-8')
                self.send_data(data_string)
            elif b'SERVICE - NEED FRAME' in message:
                self.guest_ready = True
                self.send_frame()

        else:
            if b'MOUSE_EVT' in message:
                self.play_mouse(message)
            elif b'CHAR_EVT' in message:
                self.play_char(message)
            else:
                log.warning('Unknown message: '.format(message))

    def on_error(self, *args):
        log.info("### ERROR ###")
        log.error(traceback.print_exc())
        self.error = args[1] if len(args) == 2 else args[0]
        log.error(self.error)
        self.stop()

    def on_close(self, *args):
        log.info("### CLOSED ###")
        self.stop()

    def on_open(self, *args):
        to_send = b'SERVICE - HOST START'
        log.debug(to_send)
        self.send_data(to_send)

    def run(self):
        options = dict(
            sslopt=self.settings.sslopt,
            ping_interval=2,
        )
        if self.settings.client['use_proxy']:
            log.info('Use proxy')
            options.update(
                http_proxy_host=self.settings.client['proxy_host'],
                http_proxy_port=self.settings.client['proxy_port']
            )
        self.ws.run_forever(**options)

    def stop(self):
        log.info("### STOPPED ###")
        try:
            self.ws.send(b'SERVICE - CLOSE')
        except Exception as e:
            pass
        self.forwarding = None
        self.running = None
        self.channel = None
        self.guest_ready = None

        self.ready_to_send = None
        self.last_frame_sended = None

        if self.thread_grab_image_running:
            self.thread_grab_image_running = False
            self.thread_grab_image.join()

    def send_data(self, data, opcode=websocket.ABNF.OPCODE_BINARY):
        # log.info('SENDING to websocket ({})'.format(humanbytes(len(data))))
        self.ws.send(data, opcode=opcode)

    def grab_image_loop(self, threadname):
        while self.thread_grab_image_running:
            image = self.grab_image.grab()

            if self.settings.client['colors'] != 'FULL':
                # QUANTIZE
                image = image.quantize(
                    colors=int(self.settings.client['colors']),
                    method=2,
                )
                image = image.convert('RGB')

            original = copy.deepcopy(image)
            with self.lock:
                if self.last_frame_sended:
                    diff = Image.fromarray(
                        np.array(image) - np.array(self.last_frame_sended)
                    )
                    image = diff

                payload = lz4.frame.compress(
                    image.tobytes(),
                    compression_level=9,
                    block_size=lz4.frame.BLOCKSIZE_MAX1MB,
                    return_bytearray=True,
                )
                self.ready_to_send = (payload, original, )

            time.sleep(.1)

    def send_frame(self):
        payload = None
        while True:
            with self.lock:
                if self.ready_to_send:
                    payload, self.last_frame_sended = self.ready_to_send
                    self.ready_to_send = None
                    break
            time.sleep(.1)

        if payload:
            log.debug('SEND {}'.format(humanbytes(len(payload))))
            self.ws.send(
                payload,
                opcode=websocket.ABNF.OPCODE_BINARY
            )

    def play_char(self, data):
        data = data.decode('utf-8')
        log.debug('KEYBOARD RECEIVE: ' + data)
        event_type, data = data.split(' ', 1)
        sub_type, data = data.split(' ', 1)
        if sub_type == 'CHAR':
            keyboard = KeyboardController()
            keyboard.type(data)
        elif sub_type == 'HOTKEY':
            key_event, char, num, active = data.split(' ', 3)
            if 'control_' in key_event:
                try:
                    __, char = key_event.split('_')
                    keyboard = KeyboardController()
                    keyboard.press(Key.ctrl_l)
                    keyboard.type(char)
                    keyboard.release(Key.ctrl_l)
                except Exception as e:
                    print(traceback.print_exc())
                return

            keys = []
            if 'AltDown' in active:
                keys.append(Key.alt_l)
            if 'CmdDown' in active and platform.system() == "Darwin":
                keys.append(Key.cmd_l)
            if 'ControlDown' in active:
                keys.append(Key.ctrl_l)
            if 'MetaDown' in active:
                keys.append(Key.menu)
            if 'ShiftDown' in active:
                keys.append(Key.shift_l)

            if not hasattr(Key, key_event):
                if key_event == 'pageup':
                    key_event = 'page_up'
                elif key_event == 'pagedown':
                    key_event = 'page_down'
                elif key_event == 'return':
                    key_event = 'enter'
                elif key_event == 'escape':
                    key_event = 'esc'
                elif key_event == 'back':
                    key_event = 'backspace'
            if hasattr(Key, key_event):
                keys.append(getattr(Key, key_event))
            else:
                log.error('KEY NOT VALID: {}'.format(key_event))
                return

            keyboard = KeyboardController()
            for key in keys:
                keyboard.press(key)
            for key in reversed(keys):
                keyboard.release(key)

    def play_mouse(self, mouse_event):
        data_type, data = mouse_event.split(b' ', 1)
        data_type = data_type.decode('utf-8')
        data = pickle.loads(data)
        if data_type == 'MOUSE_EVT_MOTION':
            mouse.position = data

        elif data_type == 'MOUSE_EVT_MOUSE_EVENTS':
            mouse.position = data
            mouse.press(Button.left)
        elif data_type == 'MOUSE_EVT_LEFT_UP':
            mouse.position = data
            mouse.release(Button.left)

        elif data_type == 'MOUSE_EVT_MIDDLE_DOWN':
            mouse.position = data
            mouse.press(Button.middle)
        elif data_type == 'MOUSE_EVT_MIDDLE_UP':
            mouse.position = data
            mouse.release(Button.middle)

        elif data_type == 'MOUSE_EVT_RIGHT_DOWN':
            mouse.position = data
            mouse.press(Button.right)
        elif data_type == 'MOUSE_EVT_RIGHT_UP':
            mouse.position = data
            mouse.release(Button.right)

        elif data_type == 'MOUSE_EVT_LEFT_DCLICK':
            mouse.position = data
            mouse.click(Button.left, 2)

        elif data_type == 'MOUSE_EVT_MOUSEWHEEL':
            if data[0] > 0:
                mouse.scroll(0, 10)
            else:
                mouse.scroll(0, -10)
