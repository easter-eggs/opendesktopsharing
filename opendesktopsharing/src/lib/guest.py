#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2022

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import logging
import lz4.frame
from optparse import OptionParser
import threading
import sys
import time
import traceback
import websocket
import wx

from opendesktopsharing.src.lib.commons import humanbytes
from opendesktopsharing.src.lib.commons import logging_setup
from opendesktopsharing.src.lib.settings import Settings
from opendesktopsharing.src.lib.show_capture import ShowCapture

log = logging.getLogger('OpenDesktopSharing')

try:
    _('test')
except Exception as e:
    def _(data):
        return data

class GuestWebSocket(threading.Thread):
    def __init__(self, settings, id=None, pwd=None, show_capture=None):
        super(GuestWebSocket, self).__init__()

        # websocket.enableTrace(True)
        self.show_capture = show_capture

        self.settings = settings
        self.sslopt = settings.sslopt

        self.error = None

        self.ws = websocket.WebSocketApp(
            self.settings.client['auth'],
            header=[],
            on_message=self.on_message,
            on_error=self.on_error,
            on_close=self.on_close,
            on_open=self.on_open,
        )

        self.running = True
        self.forwarding = None
        self.host_settings = None

        self.id = id
        self.pwd = pwd
        self.channel = None

        self.last_frame = None

    def on_message(self, *args):
        message = args[1] if len(args) == 2 else args[0]
        log.info('RECEIVED from websocket ({})'.format(humanbytes(len(message))))

        if b'SERVICE' in message:
            message = message.decode('utf-8')
            # COMMONS
            if 'SERVICE - CLOSE' in message:
                self.stop()
            elif 'SERVICE - MAX DURATION' in message:
                self.error = 'Max duration'
                self.stop()
            elif 'SERVICE - FORWARDING' in message:
                self.forwarding = True
            # GUEST
            elif 'SERVICE - SESSION EXPIRED' in message:
                self.error = 'Session expired'
                self.stop()
            elif 'SERVICE - SESSION NOT FOUND' in message:
                self.error = 'Session not found'
                self.stop()
            elif 'SERVICE - OK GUEST' in message:
                infos_connection = message.split()
                self.channel = infos_connection[4]
                self.forwarding = True
            elif 'SERVICE - HOST SETTINGS' in message:
                    self.host_settings = {el.split('=')[0]: el.split('=')[1] for el in message.split()[4:]}

                    self.show_capture.width = int(self.host_settings['WIDTH'])
                    self.show_capture.height = int(self.host_settings['HEIGHT'])
                    self.show_capture.setting = self.host_settings
                    self.show_capture.socket_send_data = self.send_data
                    self.show_capture.socket_stop = self.stop
                    self.show_capture.Show(True)
                    wx.CallAfter(self.show_capture.SetTitle, 'Opendesktopsharing')

        else:
            wx.CallAfter(self.build_new_frame, message)

    def on_error(self, *args):
        log.error(traceback.print_exc())
        error = args[1] if len(args) == 2 else args[0]
        log.error(error)
        self.error = error
        self.forwarding = None
        self.running = None
        self.stop()

    def on_close(self, *args):
        log.info("### CLOSED ###")
        self.forwarding = None
        self.running = None
        if self.show_capture:
            self.show_capture.Destroy()

    def on_open(self, *args):
        to_send = 'SERVICE - GUEST START: {}-{}'.format(self.id, self.pwd).encode('utf-8')
        log.debug(to_send)
        self.send_data(to_send)

    def run(self):
        options = dict(
            sslopt=self.sslopt,
            ping_interval=2,
        )
        if self.settings.client['use_proxy'] is True:
            log.info('Use proxy')
            options.update(
                http_proxy_host=self.settings.client['proxy_host'],
                http_proxy_port=self.settings.client['proxy_port']
            )
        self.ws.run_forever(**options)

    def send_data(self, data):
        # log.debug('SENDING data to websocket ({})'.format(humanbytes(len(data))))
        self.ws.send(data, opcode=websocket.ABNF.OPCODE_BINARY)

    def stop(self):
        try:
            self.ws.send(b'SERVICE - CLOSE')
        except Exception as e:
            pass
        self.forwarding = None
        self.running = None
        if self.show_capture:
            self.show_capture.Destroy()
        self.ws.close()

    def build_new_frame(self, message):
        image = message
        image = lz4.frame.decompress(image)
        self.show_capture.load_image(image)

    def send_key_to_host(self, data):
        self.send_data(data)

    def send_mouse_to_host(self, data):
        self.send_data(data)

