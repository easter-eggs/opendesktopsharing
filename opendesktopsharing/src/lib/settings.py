#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import appdirs
from certifi import where
from configparser import SafeConfigParser
import logging
import logging.handlers
import os
import platform
import ssl
from websocket import create_connection
from websocket import ABNF

from opendesktopsharing import __version__
from opendesktopsharing.src.lib import commons


log = logging.getLogger('OpenDesktopSharing')


class Settings:

    def __init__(self, config, app_mode=False):
        """Extract config parameters"""

        # Get config file
        self.config_uri = None
        self.app_mode = app_mode
        if config:
            # Use config file guestn by argument
            self.config_uri = os.path.abspath(config)
        elif os.path.exists('{}{}opendesktopsharing{}settings.ini'.format(appdirs.user_data_dir(), os.sep, os.sep)):
            # use user config file
            self.config_uri = '{}{}opendesktopsharing{}settings.ini'.format(appdirs.user_data_dir(), os.sep, os.sep)
        else:
            # use default config file
            self.config_uri = commons.get_data_file_path('config', 'settings.ini')

        if not os.path.exists(self.config_uri):
            log.error('Error, config file not found: {}'.format(self.config_uri))
            exit(2)

        if self.config_uri is None:
            self.config_uri = '{}{}settings.ini'.format(os.path.dirname(os.path.realpath(__file__)), os.sep)

        # Always show the config file in use
        print('Use config file: {}'.format(self.config_uri))

        # SETTINGS
        self.client = {
            'auth': None,
            'use_proxy': False,
            'proxy_host': None,
            'proxy_port': None,
            'colors': None,
        }
        self.server = {
            'listening_interface': None,
            'port': None,
            'download_url': None,
            'max_users': None,
            'session_max_duration': None,
            'min_id_len': None,
            'max_id_len': None,
            'min_password_len': None,
            'max_keypassword_len': None,
        }

        # PREVENT SETTINGS EDITION WHILE RUNNING
        self.force_restart = None

        try:
            log.info(self.config_uri)
            config = SafeConfigParser()
            config.read(self.config_uri)

            # Client
            self.client['auth'] = config.get('client', 'auth')
            self.client['colors'] = config.get('client', 'colors')
            if config.has_option('client', 'proxy_host') and config.has_option('client', 'proxy_port') and config.has_option('client', 'use_proxy'):
                self.client['use_proxy'] = config.getboolean('client', 'use_proxy')
                if self.client['use_proxy'] is True:
                    self.client['proxy_host'] = config.get('client', 'proxy_host')
                    self.client['proxy_port'] = config.getint('client', 'proxy_port')

            self.client['ssl_verify'] = config.getboolean('client', 'ssl_verify')
            self.client['ssl_check_cn'] = config.getboolean('client', 'ssl_check_cn')
            self.client['ssl_ca_path'] = config.get('client', 'ssl_ca_path')

            # Server
            self.server['listening_interface'] = config.get('server', 'listening_interface')
            self.server['port'] = config.getint('server', 'port')
            self.server['download_url'] = config.get('server', 'download_url')
            self.server['max_users'] = config.getint('server', 'max_users')
            self.server['session_max_duration'] = config.getint('server', 'session_max_duration')
            self.server['id_len'] = config.getint('server', 'id_len')
            self.server['password_len'] = config.getint('server', 'password_len')

            if platform.system() == "Windows":
                # TODO: Always needed ?
                # Windows FIX:
                # https://github.com/ponty/pyscreenshot/issues/25
                # Screenshots are incorrectly cropped on high-DPI displays.
                # Windows returns display geometry data scaled for the DPI, while the actual screenshots are unscaled.
                # Workaround: Right-click on python.exe, Properties, Compatibility tab, check 'Disable display scaling on high DPI settings'.
                # Repeat for pythonw.exe.
                # Using the following code inside your app makes your app DPI aware on Windows and solves the issue:
                from ctypes import windll
                user32 = windll.user32
                user32.SetProcessDPIAware()

            # Client SSL
            cafile = None
            self.ssl_ctx = None
            self.sslopt = {
            }
            if self.app_mode != 'server':
                if not self.client['ssl_check_cn']:
                    self.sslopt["check_hostname"] = False
                log.debug('CHECK SSL')
                if self.client['ssl_verify']:
                    if self.client['ssl_ca_path']:
                        log.info('Using specified ca file: {}'.format(self.client['ssl_ca_path']))
                        # Always show this message
                        print('Using specified ca file: {}'.format(self.client['ssl_ca_path']))
                        cafile = self.client['ssl_ca_path']

                    if cafile is None:
                        log.debug('Use certifi')
                        cafile = where()
                    try:
                        self.ssl_ctx = ssl.create_default_context()
                        self.ssl_ctx.load_verify_locations(cafile)
                        log.debug(os.path.isfile(cafile))
                        log.info('Using cert: {}'.format(cafile))
                    except Exception as e:
                        log.error('SSL certificat error:')
                        log.error(e)
                        self.force_restart = True
                else:
                    self.sslopt["cert_reqs"] = ssl.CERT_NONE
                    log.warning('SKIPPING CHECK SSL (ssl_verify is false in config file)')
                    # Always show this message
                    print('SKIPPING CHECK SSL (ssl_verify is false in config file)')
                log.debug('CHECK SSL: DONE')

                if self.ssl_ctx:
                    self.sslopt['context'] = self.ssl_ctx
        except Exception as e:
            log.error(e)
            log.info("Error in config file, restoring default values and exit: {}".format(e))
            self.force_restart = True

        if self.force_restart and app_mode in ['server', 'host', 'guest']:
            import sys
            log.error('Error during config load, exiting...')
            sys.exit(0)


    def save(self):
        config = SafeConfigParser()

        user_path = '{}{}opendesktopsharing'.format(appdirs.user_data_dir(), os.sep)
        if not os.path.exists(user_path):
            os.makedirs(user_path)
        config_file = open('{}/settings.ini'.format(user_path), 'w+')

        config.read('{}/settings.ini'.format(user_path))

        config.add_section('client')
        config.set('client', 'auth', self.client['auth'])
        config.set('client', 'use_proxy', str(self.client['use_proxy']).lower())
        config.set('client', 'proxy_host', self.client['proxy_host'] if self.client['proxy_host'] else '')
        config.set('client', 'proxy_port', str(self.client['proxy_port']) if self.client['proxy_port'] else '3128')
        config.set('client', 'colors', str(self.client['colors']))
        config.set('client', 'ssl_verify', str(self.client['ssl_verify']))
        config.set('client', 'ssl_check_cn', str(self.client['ssl_check_cn']))
        config.set('client', 'ssl_ca_path', str(self.client['ssl_ca_path']))

        config.add_section('server')
        config.set('server', 'listening_interface', self.server['listening_interface'])
        config.set('server', 'port', str(self.server['port']))
        config.set('server', 'download_url', self.server['download_url'])
        config.set('server', 'max_users', str(self.server['max_users']))
        config.set('server', 'session_max_duration', str(self.server['session_max_duration']))
        config.set('server', 'id_len', str(self.server['id_len']))
        config.set('server', 'password_len', str(self.server['password_len']))

        config.write(config_file)
        config_file.close()
