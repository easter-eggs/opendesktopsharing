#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


from configparser import ConfigParser
import hashlib
import logging
import logging.handlers
import math
from optparse import OptionParser
import os
import os.path
from pprint import pprint
import random
import sys
import time

from SimpleWebSocketServer import SimpleWebSocketServer as SimpleWebSocketServer_
from SimpleWebSocketServer import WebSocket as WebSocket_

from opendesktopsharing import __version__
from opendesktopsharing import __accepted_versions__


log = logging.getLogger('OpenDesktopSharing')


channels_by_objects = dict()
sessions_by_channels = dict()

class SimpleWebSocketServer(SimpleWebSocketServer_):
    def __init__(self, host, port, websocketclass, config, selectInterval=0.01):
        # calling inherited
        super(SimpleWebSocketServer, self).__init__(host, port, websocketclass, selectInterval=selectInterval)

        self.config = config

    def _constructWebSocket(self, sock, address):
        return self.websocketclass(self, sock, address, self.config)


class WebSocket(WebSocket_):
    def __init__(self, server, sock, address, config):
        self.config = config
        super(WebSocket, self).__init__(server, sock, address)

def rand_from_size(size):
    min = math.pow(10, size-1)
    max = math.pow(10, size) -1
    return random.randint(min, max)

class OpenDesktopSharingServer(WebSocket):

    def handleMessage(self):
        if self.data.startswith(b'SERVICE - '):
            if b'SERVICE - CHECK VERSION' in self.data:
                version_to_check = self.data.split(b' ')[-1]
                version_to_check_int = int(float(version_to_check))
                if version_to_check_int in __accepted_versions__:
                    self.sendMessage(b'VERSION OK')
                else:
                    self.sendMessage(b'VERSION NOK - ' + \
                        b'DOWNLOAD_URL=_=' + bytes(self.config.server['download_url'], 'utf-8') + \
                        b'__ACCEPTED_CLIENT_VERSIONS=_=' + bytes(','.join([str(el) for el in __accepted_versions__]), 'utf-8')
                    )

            # Starts HOST CONNECTION
            elif b'SERVICE - HOST START' in self.data:
                channel, key, password = None, None, None
                generate_key_password = True
                while generate_key_password is not None:
                    key = str(rand_from_size(self.config.server['id_len']))
                    password = str(rand_from_size(self.config.server['password_len']))
                    channel = str(hashlib.sha512('{}-{}'.format(key, password).encode('utf-8')).hexdigest())
                    if channel not in [key for key in sessions_by_channels.keys()]:
                        generate_key_password = None
                    else:
                        time.sleep(0.2)

                # Register channel
                channels_by_objects[self] = channel
                # Register session
                sessions_by_channels[channel] = dict()
                sessions_by_channels[channel]['HOST'] = self
                sessions_by_channels[channel]['GUEST'] = None
                sessions_by_channels[channel]['TIME'] = time.time()

                # Confirm to host
                response = 'SERVICE - START HOST SESSION: {} {} {}'.format(
                    key,
                    password,
                    channel
                )
                response = bytes(response, 'utf-8')
                sessions_by_channels[channel]['HOST'].sendMessage(response)

            # Start GUEST CONNECTION
            elif b'SERVICE - GUEST START:' in self.data:
                self.data = self.data.decode('utf-8')
                key_password = '-'.join(self.data.split(': ')[1].split('-')).encode('utf-8')
                channel = str(hashlib.sha512(key_password).hexdigest())

                # Get session
                if sessions_by_channels.get(channel):
                    channels_by_objects[self] = channel
                    sessions_by_channels[channel]['GUEST'] = self
                    response = 'SERVICE - OK GUEST: {}'.format(channel)
                    sessions_by_channels[channel]['GUEST'].sendMessage(bytes(response, 'utf-8'))
                    sessions_by_channels[channel]['HOST'].sendMessage(b'SERVICE - CONNECTED')
                else:
                    log.debug('SERVICE - SESSION NOT FOUND')
                    self.sendMessage(b'SERVICE - SESSION NOT FOUND')
                    self.close()

            # HOST SEND SETTINGS TO GUEST
            elif b'SERVICE - HOST SETTINGS' in self.data:
                channel = channels_by_objects.get(self)
                if channel:
                    session = sessions_by_channels.get(channel)
                    if session and session.get('GUEST'):
                        session['GUEST'].sendMessage(self.data)
                        # Ask first frame
                        self.sendMessage(b'SERVICE - NEED FRAME')
                        session['HOST'].sendMessage(self.data)
                    else:
                        log.debug(b'SESSION NOT FOUND')
                        self.close()
                else:
                    log.debug(b'CHANNEL NOT FOUND')
                    self.close()

            elif b'SERVICE - CLOSE' in self.data:
                self.close()
            else:
                log.warning('UNKNOWN SERVICE MESSAGE: \n{}'.format(self.data.decode('utf-8')))

        # BIDIRECTIONNAL FORWARDING
        else:
            channel = channels_by_objects.get(self)
            if channel:
                session = sessions_by_channels.get(channel)
                if session:
                    if self == session['HOST']:
                        self.sendMessage(b'SERVICE - NEED FRAME')
                        session['GUEST'].sendMessage(self.data)
                    elif self == session['GUEST']:
                        session['HOST'].sendMessage(self.data)
                else:
                    log.debug('SESSION NOT FOUND')
                    self.close()
            else:
                log.debug('CHANNEL NOT FOUND')
                self.close()

    def handleConnected(self):
        log.debug('{} connected'.format(self.address))

        # Close too long sessions
        for channel, session in sessions_by_channels.items():
            if (time.time() - session['TIME']) > float(self.config.server['session_max_duration'] * 3600):
                if session['HOST']:
                    session['HOST'].sendMessage(b'SERVICE - MAX DURATION')
                    session['HOST'].close()
                if session['GUEST']:
                    session['GUEST'].sendMessage(b'SERVICE - MAX DURATION')
                    session['GUEST'].close()

        # Check sessions count
        if len(sessions_by_channels) > self.config.server['max_users']:
            self.sendMessage(b'SERVICE - FULL')
            self.close()

    def handleClose(self):
        log.info("Closing: {} - {}".format(self.address[0], str(self.headerbuffer)))
        try:
            channel = channels_by_objects.get(self)
            if channel:
                session = sessions_by_channels.get(channel)
                if session:
                    if session['GUEST'] == self:
                        if session['HOST']:
                            session['HOST'].close()
                    if session['HOST'] == self:
                        if session['GUEST']:
                            session['GUEST'].close()
                    channels_by_objects.pop(session['GUEST'], None)
                    channels_by_objects.pop(session['HOST'], None)
                sessions_by_channels.pop(channel, None)
            log.info('ALIVE SESSIONS: {}'.format(str(len(sessions_by_channels))))
            log.info('ALIVE OBJECTS: {}'.format(str(len(channels_by_objects))))
        except Exception as e:
            log.error(e)


