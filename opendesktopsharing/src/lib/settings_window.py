#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

from urllib.parse import urlparse

import pkg_resources
import wx

from opendesktopsharing.src.lib.settings import Settings
from opendesktopsharing.src.lib import commons


class SettingsWindow(wx.Dialog):
    def __init__(self, settings, *args, **kwargs):
        wx.Dialog.__init__(self, *args, **kwargs)
        wx.Dialog.EnableLayoutAdaptation(True)
        self.SetLayoutAdaptationMode(wx.DIALOG_ADAPTATION_MODE_ENABLED)
        self.settings = settings

        self.panel = wx.Panel(self)

        self.windowSizer = wx.BoxSizer()
        self.windowSizer.Add(self.panel, 1, wx.ALL | wx.EXPAND)

        self.button_ok = wx.Button(self.panel, label=_("OK"))
        self.button_cancel = wx.Button(self.panel, label=_("Cancel"))
        self.button_reset = wx.Button(self.panel, label=_("Load defaults values"))
        self.button_ok.Bind(wx.EVT_BUTTON, self.on_ok)
        self.button_cancel.Bind(wx.EVT_BUTTON, self.on_cancel)
        self.button_reset.Bind(wx.EVT_BUTTON, self.on_reset)

        self.sizer = wx.GridBagSizer(11, 3)

        self.section_connection_label = wx.StaticText(self.panel, -1, label=_("Connection settings"))
        self.section_connection_label.SetFont(wx.Font(11, wx.DEFAULT, wx.NORMAL, wx.BOLD))
        self.sizer.Add(self.section_connection_label, (0, 0))

        self.host_quote = wx.StaticText(self.panel, -1, label=_("Server: (ws://servername:port/path)"))
        self.host_field = wx.TextCtrl(self.panel, -1, size=(300, -1))
        if settings.client['auth']:
            self.host_field.SetValue(settings.client['auth'])
        self.sizer.Add(self.host_quote, (1, 0))
        self.sizer.Add(self.host_field, (1, 1))

        self.use_proxy_quote = wx.StaticText(self.panel, -1, label=_("Proxy settings:"))
        self.use_proxy_field = wx.CheckBox(self.panel, wx.ID_ANY, _("Use proxy"))
        self.use_proxy_field.SetValue(settings.client['use_proxy'])
        self.sizer.Add(self.use_proxy_quote, (2, 0))
        self.sizer.Add(self.use_proxy_field, (2, 1))

        self.proxy_host_quote = wx.StaticText(self.panel, -1, label=_("Host:"))
        self.proxy_host_field = wx.TextCtrl(self.panel, -1, size=(300, -1))
        if settings.client['proxy_host']:
            self.proxy_host_field.SetValue(settings.client['proxy_host'])
        self.sizer.Add(self.proxy_host_quote, (3, 0))
        self.sizer.Add(self.proxy_host_field, (3, 1))

        self.proxy_port_quote = wx.StaticText(self.panel, -1, label=_("Port:"))
        self.proxy_port_field = wx.SpinCtrl(self.panel, -1, size=(-1, -1))
        self.proxy_port_field.SetRange(minVal=1, maxVal=65535)
        if settings.client['proxy_port']:
            self.proxy_port_field.SetValue(settings.client['proxy_port'])
        else:
            self.proxy_port_field.SetValue(3128)
        self.sizer.Add(self.proxy_port_quote, (4, 0))
        self.sizer.Add(self.proxy_port_field, (4, 1))

        self.section_client_label = wx.StaticText(self.panel, -1, label=_("Quality settings (Host side)"))
        self.section_client_label.SetFont(wx.Font(11, wx.DEFAULT, wx.NORMAL, wx.BOLD))
        self.sizer.Add(self.section_client_label, (5, 0))

        self.Bind(wx.EVT_CHECKBOX, self.on_check_proxy_field, self.use_proxy_field)
        self.on_check_proxy_field(None)


        self.colors_depth = ['FULL', '256']
        max = 256
        while max > 2:
            max /= 2
            self.colors_depth.append(str(int(max)))
        self.colors_quote = wx.StaticText(self.panel, -1, label=_("Colors depth:"))
        self.colors_field = wx.ComboBox(self.panel, -1, size=(100, -1), choices=self.colors_depth, style=wx.CB_READONLY)
        if settings.client['colors']:
            self.colors_field.SetSelection(self.colors_depth.index(str(settings.client['colors'])))
        self.sizer.Add(self.colors_quote, (6, 0))
        self.sizer.Add(self.colors_field, (6, 1))

        self.sizer.Add(self.button_ok, (8, 0))
        self.sizer.Add(self.button_reset, (8, 1))
        self.sizer.Add(self.button_cancel, (8, 2))

        self.border = wx.BoxSizer()
        self.border.Add(self.sizer, 1, wx.ALL | wx.EXPAND, 10)

        wx.CallAfter(self.panel.SetSizerAndFit, self.border)
        wx.CallAfter(self.SetSizerAndFit, self.windowSizer)

    def on_check_proxy_field(self, event):
        if self.use_proxy_field.GetValue() is True:
            self.proxy_host_field.Enable()
            self.proxy_port_field.Enable()
        else:
            self.proxy_host_field.Disable()
            self.proxy_port_field.Disable()

    def on_cancel(self, e):
        self.EndModal(wx.ID_CANCEL)

    def on_close(self, e):
        self.EndModal(wx.ID_CLOSE)

    def on_ok(self, e):
        errors = None

        parsed_url = urlparse(self.host_field.GetValue())
        if not (parsed_url.scheme and parsed_url.geturl()):
            self.host_field.SetBackgroundColour(wx.RED)
            errors = True
        if self.use_proxy_field.GetValue() is True:
            if self.proxy_host_field.GetValue() == '':
                self.proxy_host_field.SetBackgroundColour(wx.RED)
                errors = True
        if errors is True:
            return None

        self.settings.client['auth'] = self.host_field.GetValue()
        self.settings.client['colors'] = self.colors_depth[self.colors_field.GetSelection()]

        self.settings.client['use_proxy'] = self.use_proxy_field.GetValue()
        if self.use_proxy_field.GetValue() is True:
            self.settings.client['proxy_host'] = self.proxy_host_field.GetValue()
            self.settings.client['proxy_port'] = int(self.proxy_port_field.GetValue())

        self.EndModal(wx.ID_OK)

    def on_reset(self, e):
        config_uri = commons.get_data_file_path('config', 'settings.ini')
        self.default_settings = Settings(config_uri)
        # CLIENT
        self.settings.client['auth'] = self.default_settings.client['auth']
        self.settings.client['colors'] = self.default_settings.client['colors']
        self.settings.client['use_proxy'] = self.default_settings.client['use_proxy']
        self.settings.client['proxy_host'] = self.default_settings.client['proxy_host']
        self.settings.client['proxy_port'] = self.default_settings.client['proxy_port']
        self.settings.client['ssl_verify'] = self.default_settings.client['ssl_verify']
        self.settings.client['ssl_check_cn'] = self.default_settings.client['ssl_check_cn']
        self.settings.client['ssl_ca_path'] = self.default_settings.client['ssl_ca_path']

        # SERVER
        self.settings.server['listening_interface'] = self.default_settings.server['listening_interface']
        self.settings.server['port'] = self.default_settings.server['port']
        self.settings.server['download_url'] = self.default_settings.server['download_url']
        self.settings.server['max_users'] = self.default_settings.server['max_users']
        self.settings.server['session_max_duration'] = self.default_settings.server['session_max_duration']
        self.settings.server['id_len'] = self.default_settings.server['id_len']
        self.settings.server['password_len'] = self.default_settings.server['password_len']

        self.host_field.SetValue(self.settings.client['auth'])
        self.colors_field.SetValue(str(self.settings.client['colors']))
        self.use_proxy_field.SetValue(self.settings.client['use_proxy'])
        if self.settings.client['use_proxy'] is True:
            self.proxy_host_field.SetValue(self.settings.client['host_proxy'])
            self.proxy_port_field.SetValue(self.settings.client['port_proxy'])

        self.on_check_proxy_field(None)

    def get_settings(self):
        return self.settings
