#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""

__version__ = 2.0
__accepted_versions__ = [
    1, 2
]

APP_NAME = "OpenDesktopSharing"
COPYRIGHT = "(C) 2021 Easter Eggs"
WEBSITE = "https://www.opendesktopsharing.org"
DEVELOPER = "parnaud@easter-eggs.com"
COMPANY = "Easter-eggs"

INFO_LICENCE = """
OpenDesktopSharing

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
"""

NEW_VERSION_ALERT = """Server error:
Your version is {} but the server accepts only {}
Please check {}"""

ODS_DESCRIPTION_TXT = """OpenDesktopSharing (Client version)


The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

"""