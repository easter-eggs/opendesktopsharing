# -*- mode: python -*-

block_cipher = None


a = Analysis(['opendesktopsharing/src/main.py'],
             pathex=['opendesktopsharing/src', 'opendesktopsharing/src/lib', 'opendesktopsharing/src/share'],
             binaries=[
             ],
             datas=[
                ('opendesktopsharing/share/images/get_info-mini.png', './opendesktopsharing/share/images', ),
                ('opendesktopsharing/share/images/exit-mini.png', './opendesktopsharing/share/images', ),
                ('opendesktopsharing/share/images/preferences_system-mini.png', './opendesktopsharing/share/images/', ),
                ('opendesktopsharing/share/images/logo-medium.png', './opendesktopsharing/share/images', ),
                ('opendesktopsharing/share/images/odc.ico', './opendesktopsharing/share/images',),
                ('opendesktopsharing/share/config/settings.ini', './opendesktopsharing/share/config',),
                ('opendesktopsharing/share/locale', './opendesktopsharing/share/locale'),
                ('README.md', './opendesktopsharing/share/'),
                ('opendesktopsharing/share/license/gpl-3.0.txt', './opendesktopsharing/share/license'),
                ('/usr/share/icons', './share/icons'),],
             hiddenimports=["pynput.keyboard._xorg", "pynput.mouse._xorg", "pynput.keyboard._uinput"],
             hookspath=[],
             runtime_hooks=[],
             excludes=[
             ],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='OpenDesktopSharing',
          debug=False,
          strip=True,
          upx=False,
          console=False, )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=True,
               upx=False,
               console=False,
               name='OpenDesktopSharing')
app = BUNDLE(coll,
             console=False,
             name='OpenDesktopSharing_',
             icon='./opendesktopsharing/share/images/odc.ico',
             bundle_identifier=None)