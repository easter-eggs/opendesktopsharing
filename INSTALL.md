# Install (GNU/Linux)

## Only the server (All OS)

    apt install \
        git \
        python3-pip \
        python3-setuptools \
        python3-setuptools-scm \
        python3-pkgconfig \
        python3-appdirs \
        python3-websocket \
    $ pip3 install -e '.[server]'

## Client and server

### DEBIAN 11

    $ su -
    $ apt install \
        git \
        python3-pip \
        python3-setuptools \
        python3-wxgtk4.0 \
        python3-appdirs \
        python3-numpy \
        python3-lz4 \
        python3-pil \
        python3-babel
    $ exit
    $ git clone https://framagit.org/easter-eggs/opendesktopsharing.git
    $ cd opendesktopsharing
    $ pip3 install -e '.[debian11,server]' --user
    $ ~/.local/bin/opendesktopsharing

### DEBIAN 10

    $ su -
    $ apt install \
        git \
        python3-pip \
        python3-setuptools \
        python3-wxgtk4.0 \
        python3-appdirs \
        python3-websocket \
        python3-numpy \
        python3-lz4 \
        python3-pil \
        python3-babel
    $ exit
    $ git clone https://framagit.org/easter-eggs/opendesktopsharing.git
    $ cd opendesktopsharing
    $ pip3 install -e '.[debian10,server]' --user
    $ ~/.local/bin/opendesktopsharing

### DEBIAN 9

    $ su -
    $ apt install \
        git \
        python3-pip \
        python3-setuptools \
        python3-setuptools-scm \
        python3-pkgconfig \
        python3-appdirs \
        python3-websocket \
        python3-numpy \
        python3-babel \
        libsdl2-mixer-2.0-0 \
        libsdl2-2.0-0
    $ exit
    $ pip3 install -U -f https://extras.wxpython.org/wxPython4/extras/linux/gtk3/debian-9 wxPython --user
    $ git clone https://framagit.org/easter-eggs/opendesktopsharing.git
    $ cd opendesktopsharing
    $ pip3 install -e '.[debian9,server]' --user
    $ ~/.local/bin/opendesktopsharing

### Windows 10

- Get Python 3.9.0: https://www.python.org/ftp/python/3.9.0/python-3.9.0-amd64.exe
- Get Git bash

    $ git clone https://framagit.org/easter-eggs/opendesktopsharing.git
    $ cd opendesktopsharing
    $ python.exe -m pip install --upgrade pip setuptools
    $ python.exe -m pip install -e .[full,server] --user
    $ python.exe -m opendesktopsharing.src.main

### macOs

- Get Python 3.7.9: https://www.python.org/ftp/python/3.7.9/python-3.7.9-macos.10.9.pkg
- Get Git

    $ git clone https://framagit.org/easter-eggs/opendesktopsharing.git
    $ cd opendesktopsharing
    $ python -m pip install -e .[full,server] --user
    $ python -m opendesktopsharing.src.main

## Pyinstaller

**On Linux zlib must be installed**

    $ apt install zlib1g-dev


### Install pyinstaller and build with the spec file:

    $ python3 -m pip install -U pip
    $ python3 -m pip install -U setuptools
    $ python3 -m pip install pyinstaller
    $ pyinstaller pyinstaller-XXX.spec

