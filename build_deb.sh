#!/bin/bash

# Enter source directory
cd $( dirname $0 )

# Clean previous build
rm -fr deb_dist

# Set version in pyproject.toml
VERSION_SUFFIX="~ee$( lsb_release -r -s )0+1"
VERSION="$( git describe --tags|sed 's/^[^0-9]*//' )${VERSION_SUFFIX}"
sed -i "s/^__version__ *=.*$/__version__ = '$VERSION'/" opendesktopsharing/__init__.py

# Build debian source package
python3 setup.py --command-packages=stdeb.command sdist_dsc \
	--maintainer "Easter-eggs <info@easter-eggs.com>" \
	--section net \
	--forced-upstream-version "$VERSION"

# Check gitdch is installed
GITDCH=$(which gitdch)
set -e
if [ -z "$GITDCH" ]
then
        TMP_GITDCH=$(mktemp -d)
        echo "Temporary install gitdch in $TMP_GITDCH"
        git clone https://gogs.zionetrix.net/bn8/gitdch.git $TMP_GITDCH/gitdch
        GITDCH=$TMP_GITDCH/gitdch/gitdch
else
        TMP_GITDCH=""
fi

# Keep only debian package directory and orig.tar.gz archive
find deb_dist/ -maxdepth 1 -type f ! -name '*.orig.tar.gz' -delete

# Enter in debian package directory
cd deb_dist/opendesktopsharing-$VERSION

# Patch to install icon
cat << EOF >> debian/opendesktopsharing.install
opendesktopsharing.svg usr/share/icons/hicolor/scalable/apps/
EOF

# Generate debian changelog using generate_debian_changelog.py
$GITDCH \
        --package-name opendesktopsharing \
        --version "${VERSION}" \
	--version-suffix "${VERSION_SUFFIX}" \
        --code-name $( lsb_release -c -s ) \
        --output debian/changelog \
        --path ../../ \
        --verbose

# Clean temporary gitdch installation
[ -n "$TMP_GITDCH" ] && rm -fr $TMP_GITDCH

# Build debian package
dpkg-buildpackage --no-sign
