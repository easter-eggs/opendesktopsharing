#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""
OpenDesktopSharing



The OpenDesktopSharing's goal is to access to remote computer across firewall and proxy:
- No needed to know IPs address
- Access only need KEY/PASSWORD
- Use websocket VPN to connect server and client
- Use Auth server (OpenDesktopSharing-Server) to generate auth access

@author: Pierre Arnaud <parnaud@easter-eggs.com>
@copyright: 2021

OpenDesktopSharing is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

OpenDesktopSharing is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with OpenDesktopSharing.  If not, see <http://www.gnu.org/licenses/>.
"""


import io
import os

from setuptools import find_packages, setup
from distutils.command.build import build
import opendesktopsharing

build.sub_commands.insert(0, ('compile_catalog', None))

here = os.path.abspath(os.path.dirname(__file__))
with io.open(os.path.join(here, 'README.md'), mode='r', encoding='utf-8') as fd:
    README = fd.read()
with io.open(os.path.join(here, 'CHANGES.txt'), mode='r', encoding='utf-8') as fd:
    CHANGES = fd.read()
version = opendesktopsharing.__version__
requires = [
    'appdirs>=1.4.0',
    'babel',
    'certifi',
    'lz4>=3.1.10',
    'mss>=6.1.0',
    'numpy>=1.18.5',
    'Pillow>=5.4.1',
    'pynput>=1.7.6',
    'websocket-client>=0.37.0',
    'pyscreenshot',
]
extras = {
    'server': [
        'SimpleWebSocketServer',
    ],
    'wxpython': [
        'wxpython>=4.0.7',
    ],
}
data_dir = "opendesktopsharing"
data_files = [
    ("share/opendesktopsharing/images",  [
        os.path.join(data_dir, "share/images/exit-mini.png"),
        os.path.join(data_dir, "share/images/get_info-mini.png"),
        os.path.join(data_dir, "share/images/logo-medium.png"),
        os.path.join(data_dir, "share/images/preferences_system-mini.png"),
        os.path.join(data_dir, "share/images/odc.ico"),
    ]),
    ("share/opendesktopsharing/config",  [
        os.path.join(data_dir, "share/config/settings.ini"),
    ]),
    ("share/opendesktopsharing/license",  [
        os.path.join(data_dir, "share/license/gpl-3.0.txt"),
    ]),
    ("share/opendesktopsharing/locale",  [
        os.path.join(data_dir, "share/locale/opendesktopsharing.pot"),
    ]),
    ("share/opendesktopsharing/locale/fr/LC_MESSAGES",  [
        os.path.join(data_dir, "share/locale/fr/LC_MESSAGES/opendesktopsharing.po"),
        os.path.join(data_dir, "share/locale/fr/LC_MESSAGES/opendesktopsharing.mo"),
    ])
]

setup(
    name='opendesktopsharing',
    version=str(version),
    description="Access to remote desktop across firewall and proxy",
    long_description=README + '\n\n' + CHANGES,
    classifiers=[
        "Programming Language :: Python",
        "Topic :: Internet :: WWW/HTTP",
    ],
    author='Pierre ARNAUD',
    author_email='parnaud@easter-eggs.com',
    url='https://www.opendesktopsharing.org',
    keywords='remote desktop distant access tunnel websocket',
    data_files=data_files,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=True,
    install_requires=requires,
    extras_require=extras,
    python_requires='>=3.5',
    entry_points="""\
    [console_scripts]
    opendesktopsharing = opendesktopsharing.src.main:main
    """,
    message_extractors = {
      './': [
          ('**.py', 'python', None),
          ('opendesktopsharing/**.py', 'python', None),
          ('opendesktopsharing/src/**.py', 'python', None),
          ('opendesktopsharing/src/lib/**.py', 'python', None),
      ]
    },
)
