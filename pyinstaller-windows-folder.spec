# -*- mode: python -*-

block_cipher = None


a = Analysis(['opendesktopsharing/src/main.py'],
             pathex=['opendesktopsharing\\src', 'opendesktopsharing\\src\\lib'],
             binaries=[
                ('opendesktopsharing\\share\\images\\get_info-mini.png', '.\\opendesktopsharing\\share\\images', ),
                ('opendesktopsharing\\share\\images\\exit-mini.png', '.\\opendesktopsharing\\share\\images', ),
                ('opendesktopsharing\\share\\images\\preferences_system-mini.png', '.\\opendesktopsharing\\share\\images\\', ),
                ('opendesktopsharing\\share\\images\\logo-medium.png', '.\\opendesktopsharing\\share\\images', ),
                ('opendesktopsharing\\share\\images\\odc.ico', '.\\opendesktopsharing\\share\\images',),
                ('opendesktopsharing\\share\\config\\settings.ini', '.\\opendesktopsharing\\share\\config',),
                ('opendesktopsharing\\share\\locale', '.\\opendesktopsharing\\share\\locale'),
                ('README.md', '.\\opendesktopsharing\\data'),
                ('opendesktopsharing\\share\\license\\gpl-3.0.txt', '.\\opendesktopsharing\\share\\license'),
             ],
             datas=[],
             hiddenimports=[
             ],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          name='OpenDesktopSharing',
          icon='opendesktopsharing\\share\\images\\odc.ico',
          debug=False,
          strip=False,
          upx=False,
          console=True)
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=False,
               name='OpenDesktopSharing')
