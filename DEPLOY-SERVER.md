# Deploy a server

This document explains how to deploy an OpenDesktopSharing server easily using precompiled / all-in-one binary on Debian GNU/Linux Bullseye (11).

__Note:__ The installation on older versions of Debian GNU/Linux is also possible (since version 9, Strech) : just adapt the precompiled / all-in-one binary download URL.

## Prepare environment

Create the `ods` user :
```bash
adduser --system --gecos 'OpenDesktopSharing' --disabled-password \
    --home /srv/ods ods
```

## Deploy the service

Download precompiled / all-in-one binary and the example configuration file:
```bash
su - ods
mkdir apps config
wget -O apps/OpenDesktopSharing https://opendesktopsharing.easter-eggs.com/download/1.1/debian-11/OpenDesktopSharing
chmod +x apps/OpenDesktopSharing
wget -O config/settings.example.ini https://framagit.org/easter-eggs/opendesktopsharing/-/raw/master/opendesktopsharing/share/config/settings.ini
cp config/settings.example.ini config/settings.ini
```

You could adapt the `[server]` section of the configuration file `config/settings.ini` if need.

Deploy the systemd unit file in `/etc/systemd/system/ods.service`:

```ini
[Unit]
Description=OpenDesktopSharing Server
After=syslog.target network.target

[Service]
Type=simple
User=ods
Group=ods
WorkingDirectory=/home/ods
ExecStart=/home/ods/apps/OpenDesktopSharing \
	--debug \
	--config /home/ods/config/settings.ini \
	--server
Restart=on-abort

[Install]
WantedBy=multi-user.target
```

Enable service on boot and start it:
```bash
systemctl daemon-reload
systemctl enable ods
systemctl start ods
```

## Deploy Apache reverse proxy

Install Apache2 httpd:
```bash
apt install apache2
```

Enable `proxy_wstunnel` module:
```bash
a2enmod proxy_wstunnel
service apache2 restart
```

### Deploy in a sub-url

Create the configuration file `/etc/apache2/conf-available/ods.conf`:
```
ProxyRequests Off
ProxyPreserveHost on
ProxyPass /ods ws://127.0.0.1:9000 retry=0 max=20 ttl=60 acquire=10
ProxyPassReverse /ods ws://127.0.0.1:9000`
```

Enable it :
```bash
a2enconf ods
service apache2 reload
```

## Deploy in a dedicated VirtualHost

Create the configuration file `/etc/apache2/sites-available/ods.conf`:
```
<VirtualHost *:443>
        ServerName ods.example.net

        SSLEngine on

        DocumentRoot /var/www/empty

        ProxyRequests Off
        ProxyPreserveHost on

        ProxyPass /.well-known !

        ProxyPass / ws://127.0.0.1:9000 retry=0 max=20 ttl=60 acquire=10
        ProxyPassReverse / ws://127.0.0.1:9000

        <Location />
                Require all granted
        </Location>

        ErrorLog  /var/log/apache2/ods.example.net-error.log
        CustomLog /var/log/apache2/ods.example.net-access.log combined
</VirtualHost>
```

Enable it :
```bash
a2ensite ods
service apache2 reload
```
